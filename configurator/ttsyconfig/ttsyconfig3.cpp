/*----------------------------------------------------------*/
/*                                                          */
/*   Turbo Vision TVDEMO source file                        */
/*                                                          */
/*----------------------------------------------------------*/
/*
 *      Turbo Vision - Version 2.0
 *
 *      Copyright (c) 1994 by Borland International
 *      All Rights Reserved.
 *
 */

#define Uses_TRect
#define Uses_TMenuBar
#define Uses_TSubMenu
#define Uses_TMenuItem
#define Uses_TKeys
#define Uses_fpstream
#define Uses_TView
#define Uses_TPalette
#define Uses_MsgBox
#define Uses_TFileDialog
#define Uses_TApplication
#define Uses_TStringCollection
#define Uses_TDeskTop
#define Uses_TStaticText
#define Uses_TDialog
#define Uses_TEventQueue

#include <tvision/tv.h>

#include "ttsyconfig.h"
#include "tvcmds.h"
#include "gadgets.h"
#include "mousedlg.h"
#include "demohelp.h"
#include "fileview.h"
#include "updlist.h"
#include <tvision/help.h>

//
// Mouse Control Dialog Box function
//

void TVDemo::mouse()
{
    TMouseDialog *mouseCage = (TMouseDialog *) validView( new TMouseDialog() );

    if (mouseCage != 0)
        {
        mouseCage->helpCtx = hcOMMouseDBox;
        mouseCage->setData(&(TEventQueue::mouseReverse));
        if (deskTop->execView(mouseCage) != cmCancel)
            mouseCage->getData(&(TEventQueue::mouseReverse));
        }
    destroy( mouseCage );
   
}

void TVDemo::updateDistrCmd()
{
    TMinioDialog *d= (TMinioDialog *)validView(
        new TMinioDialog("bluedaemon", deskTop));

    if( d != 0 && deskTop->execView( d ) != cmCancel )
    {
    }
    destroy( d );
}

//
// File Viewer function
//

void TVDemo::openFile( const char *fileSpec )
{
    TFileDialog *d= (TFileDialog *)validView(
    new TFileDialog(fileSpec, "Открыть файл", "~И~мя", fdOpenButton, 100 ));

    if( d != 0 && deskTop->execView( d ) != cmCancel )
        {
        char fileName[MAXPATH];
        d->getFileName( fileName );
        d->helpCtx = hcFOFileOpenDBox;
        TView *w= validView( new TFileWindow( fileName ) );
        if( w != 0 )
            deskTop->insert(w);
    }
    destroy( d );
}


//
// "Out of Memory" function ( called by validView() )
//

void TVDemo::outOfMemory()
{
    messageBox( "Not enough memory available to complete operation.",
      mfError | mfOKButton );
}

//
// getPalette() function ( returns application's palette )
//
/*
TPalette& TVDemo::getPalette() const
{
    static TPalette newcolor ( cpAppColor cHelpColor, sizeof( cpAppColor cHelpColor )-1 );
    static TPalette newblackwhite( cpAppBlackWhite cHelpBlackWhite, sizeof( cpAppBlackWhite cHelpBlackWhite)-1 );
    static TPalette newmonochrome( cpAppMonochrome cHelpMonochrome, sizeof( cpAppMonochrome cHelpMonochrome)-1 );
    static TPalette *palettes[] =
        {
        &newcolor,
        &newblackwhite,
        &newmonochrome
        };
    return *(palettes[appPalette]);

}
*/

//
// isTileable() function ( checks a view on desktop is tileable or not )
//

static Boolean isTileable(TView *p, void * )
{
   if( (p->options & ofTileable) != 0)
       return True;
   else
       return False;
}

//
// idle() function ( updates heap and clock views for this program. )
//

void TVDemo::idle()
{
    TProgram::idle();
    clock->update();
    if (deskTop->firstThat(isTileable, 0) != 0 )
        {
        enableCommand(cmTile);
        enableCommand(cmCascade);
        }
    else 
        {
        disableCommand(cmTile);
        disableCommand(cmCascade);
        }
}

//
// closeView() function
//

static void closeView(TView *p, void *p1)
{
    message(p, evCommand, cmClose, p1);
}

//
// loadDesktop() function 
//

void TVDemo::loadDesktop(fpstream &s)
{
    TView  *p;

    if (deskTop->valid(cmClose))
        { 
        deskTop->forEach(::closeView, 0);  // Clear the desktop
        do {
           s >> p;
           deskTop->insertBefore(validView(p), deskTop->last);
           }
           while (p != 0);
        }
}

//
// Menubar initialization.
//

TMenuBar *TVDemo::initMenuBar(TRect r)
{
    TSubMenu& sub1 =
      *new TSubMenu( "~\360~", 0, hcSystem ) +
        *new TMenuItem( "~В~идео режим", cmVideoMode, kbNoKey, hcNoContext, "" ) +
         newLine() +
        *new TMenuItem( "~О~ программе...", cmAboutCmd, kbNoKey, hcSAbout );

    TSubMenu& sub2 =
      *new TSubMenu( "~Ф~айл", 0, hcFile ) +
        *new TMenuItem( "~О~ткрыть...", cmOpenCmd, kbF3, hcFOpen, "F3" ) +
        *new TMenuItem( "~С~менить каталог...", cmChDirCmd, kbNoKey, hcFChangeDir ) +
         newLine() +
        *new TMenuItem( "Вы~х~од", cmQuit, kbAltX, hcFExit, "Alt-X" );

    TSubMenu& sub3 =
      *new TSubMenu( "О~к~на", 0, hcWindows ) +
        *new TMenuItem( "~П~еремещение/Размер", cmResize, kbCtrlF5, hcWSizeMove, "Ctrl-F5" ) +
        *new TMenuItem( "~Р~аспахнуть", cmZoom, kbF5, hcWZoom, "F5" ) +
        *new TMenuItem( "~С~ледующее", cmNext, kbF6, hcWNext, "F6" ) +
        *new TMenuItem( "~З~акрыть", cmClose, kbAltF3, hcWClose, "Alt-F3" ) +
        *new TMenuItem( "П~л~итка", cmTile, kbNoKey, hcWTile ) +
        *new TMenuItem( "~К~аскад", cmCascade, kbNoKey, hcWCascade );

    TSubMenu& sub4 =
      *new TSubMenu( "~П~араметры", 0, hcOptions ) +
        *new TMenuItem( "~М~ышь...", cmMouseCmd, kbNoKey, hcOMouse ) +
        *new TMenuItem( "~Ц~вета...", cmColorCmd, kbNoKey, hcOColors ) +
        *new TMenuItem( "П~о~дложка...", cmChBackground, kbNoKey ) +
        *new TMenuItem( "О~б~новления...", cmUpdates_Cmd, kbNoKey ) +
        (TMenuItem&) (
            *new TSubMenu( "~Р~абочий стол", 0 ) +
            *new TMenuItem( "~С~охранить рабочий стол", cmSaveCmd, kbNoKey, hcOSaveDesktop ) +
            *new TMenuItem( "~В~осстановить рабочий стол", cmRestoreCmd, kbNoKey, hcORestoreDesktop )
        );

    r.b.y =  r.a.y + 1;
    return (new TMenuBar( r, sub1 + sub2 + sub3 + sub4 ) );
}



