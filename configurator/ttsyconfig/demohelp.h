#if !defined( __DEMOHELP_H )
#define __DEMOHELP_H

const int
  hcCancelBtn            = 26,
  hcFCChDirDBox          = 28,
  hcFChangeDir           = 7,
  hcFExit                = 8,
  hcFOFileOpenDBox       = 22,
  hcFOFiles              = 24,
  hcFOName               = 23,
  hcFOOpenBtn            = 25,
  hcFOpen                = 6,
  hcFile                 = 5,
  hcNocontext            = 0,
  hcOCColorsDBox         = 30,
  hcOColors              = 19,
  hcOMMouseDBox          = 29,
  hcOMouse               = 18,
  hcORestoreDesktop      = 21,
  hcOSaveDesktop         = 20,
  hcOpenBtn              = 27,
  hcOptions              = 17,
  hcSAbout               = 4,
  hcSystem               = 3,
  hcViewer               = 2,
  hcWCascade             = 13,
  hcWClose               = 16,
  hcWNext                = 14,
  hcWPrevious            = 15,
  hcWSizeMove            = 10,
  hcWTile                = 12,
  hcWZoom                = 11,
  hcWindows              = 9;

#endif
