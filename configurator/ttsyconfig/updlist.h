#if !defined( __UPDLIST_H )
#define __UPDLIST_H

#include <s3.h>

class TS3Loader
{
public:
    TS3Loader(const std::string &endpoint, const std::string &kid, const std::string &sk);
    bool getFile(const std::string &bkt, const std::string &key);
    TStringCollection *getBucketList(const std::string &bkt);
private:
    S3Client s3client;
};

class _FAR TListBox;

class TMinioDialog : public TDialog
{
public:
    TMinioDialog(std::string bkt, TDeskTop *b);
    virtual void handleEvent( TEvent& );
    void changeDir();
private:
    void setUpDialog();
    std::string bucket_name;
    TListBox *bList;
    TDeskTop *backGround;
    TS3Loader loader;
};


#endif // __UPDLIST_H
