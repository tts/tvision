#define Uses_TRect
#define Uses_TStaticText
#define Uses_TEvent
#define Uses_TDrawBuffer
#define Uses_TDialog
#define Uses_TFileInputLine
#define Uses_TDeskTop
#define Uses_TLabel
#define Uses_TListBox
#define Uses_TScrollBar
#define Uses_TStringCollection
#define Uses_TCheckBoxes
#define Uses_TChDirDialog
#define Uses_MsgBox
#define Uses_TButton
#define Uses_TSItem
#define Uses_TEventQueue
#include <tvision/tv.h>

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <strstrea.h>
#include "updlist.h"

#define MAXPATH		512
const int cmSelectDir    = 100;
const int cmLoad         = 101;

const std::string current_kid = "intellimech";
const std::string current_sk  = "bluedaemonstorage";
const std::string current_endpoint = "https://iotool.ttsy.ru:9000";

TS3Loader::TS3Loader(const std::string &endpoint, const std::string &kid, const std::string &sk) :
    s3client(endpoint, kid, sk)
{
}

bool TS3Loader::getFile(const std::string &bkt, const std::string &key) {
  bool result = true;
  S3ClientIO objinfo_io;
  setlocale(LC_TIME, "C");
  s3client.StatObject(bkt, key, objinfo_io);

  ofstream fout(key.c_str(), ios_base::binary | ios_base::out);
  S3ClientIO io(NULL, &fout);
  io.bytesToGet = objinfo_io.respHeaders.GetWithDefault("Content-Length", 0);
  s3client.GetObject(bkt, key, io);
  setlocale(LC_TIME, "");

  if(io.Failure()) {
      messageBox("ERROR: failed to get object", mfError | mfOKButton );
      result = false;
  }
  setlocale(LC_TIME, "");
  return result;
}

TStringCollection *TS3Loader::getBucketList(const std::string &bkt) {
    Minio::S3::Bucket bucket(bkt, "");

    setlocale(LC_TIME, "C");
    TStringCollection *result = new TStringCollection(5, 5);
    s3client.ListObjects(bucket);

    std::list<Minio::S3::Object>::iterator it;
    for (it = bucket.objects.begin(); it != bucket.objects.end(); ++it) {
        std::string str = (*it).key;
        char *name = new char[str.length()+1];
        std::strcpy(name, str.c_str());
        result->insert(name);
    }
    setlocale(LC_TIME, "");
    return result;
}

TMinioDialog::TMinioDialog(std::string bkt, TDeskTop *b) :
    TWindowInit( &TMinioDialog::initFrame ),
    TDialog( TRect(0, 0, 38, 22), "Список обновлений" ),
    backGround(b),
    loader(current_endpoint, current_kid, current_sk)
{
    options |= ofCentered;
    bucket_name = bkt;

    TScrollBar *sb = new TScrollBar( TRect( 33, 4, 34, 15 ) );
    bList = new TListBox( TRect( 3, 4, 33, 15 ), 1, sb );

    insert( sb );
    insert( bList );

    TFileInputLine *fline = new TFileInputLine( TRect( 3, 17, 29, 18 ), MAXPATH );
    insert(fline);
    insert( new TLabel( TRect( 2, 16, 23, 17 ), "Сохранить в каталог", fline ) );
    insert(new TButton( TRect(30, 17, 35, 19), "O", cmSelectDir, bfNormal));


    insert( new TLabel( TRect( 2, 2, 21, 3 ), "Список обновлений", bList) );
    insert(new TButton( TRect(9, 19, 20, 21), "С~к~ачать", cmLoad, bfDefault));
    insert(new TButton( TRect(22, 19, 32, 21), "Отмена", cmCancel, bfNormal));

    try {
       TStringCollection *slist = loader.getBucketList(bucket_name);
       bList->newList(slist);
    } catch (...) {
    }

    selectNext( (Boolean) 0);
}

void TMinioDialog::changeDir()
{
    TView *d = (TView*)new TChDirDialog( 0, cmSelectDir );
    if( d != 0 )
    {
        backGround->execView( d );
        destroy( d );
    }
}

void TMinioDialog::handleEvent(TEvent& event)
{
    TDialog::handleEvent(event);
    if (event.what == evCommand)
    {
        switch (event.message.command)
        {
            case cmSelectDir:
                changeDir();
                break;
            case cmLoad:
                char *p = (char *)bList->list()->at( bList->focused );
                loader.getFile(bucket_name, std::string(p));
                break;
        }
        clearEvent(event);
    }
}


