/*----------------------------------------------------------*/
/*                                                          */
/*   Turbo Vision TVDEMO source file                        */
/*                                                          */
/*----------------------------------------------------------*/
/*
 *      Turbo Vision - Version 2.0
 *
 *      Copyright (c) 1994 by Borland International
 *      All Rights Reserved.
 *
 */

#define Uses_TDialog
#define Uses_TRect
#define Uses_TStaticText
#define Uses_TButton
#define Uses_TEvent
#define Uses_TWindow
#define Uses_TColorGroup
#define Uses_TColorItem
#define Uses_TColorDialog
#define Uses_TPalette
#define Uses_TDeskTop
#define Uses_TApplication
#define Uses_TChDirDialog
#define Uses_TScreen

#include <tvision/tv.h>

#include "ttsyconfig.h"
#include "tvcmds.h"
#include "demohelp.h"
#include "backgrnd.h"

#include <stdlib.h>
#include <signal.h>

//
// DemoApp::handleEvent()
//  Event loop to distribute the work.
//

void TVDemo::handleEvent(TEvent &event)
{
    TApplication::handleEvent(event);

    if (event.what == evCommand)
    {
        switch (event.message.command)
            {
            case cmAboutCmd:            //  About Dialog Box
                aboutDlgBox();
                break;

            case cmChBackground:        //  Change Background pattern
                chBackground();
                break;

            case cmOpenCmd:             //  View a file
                openFile("*.*");
                break;

            case cmChDirCmd:            //  Change directory
                changeDir();
                break;

            case cmMouseCmd:            //  Mouse control dialog box
                mouse();
                break;

            case cmColorCmd:            //  Color control dialog box
                colors();
                break;

        case cmSaveCmd:             //  Save current desktop
                saveDesktop();
                break;
 
        case cmRestoreCmd:          //  Restore saved desktop
                retrieveDesktop();
                break;

        case cmUpdates_Cmd:
                updateDistrCmd();
            default:                    //  Unknown command
                return;

            }
        clearEvent (event);
        }
}



//
// About Box function()
//

void TVDemo::aboutDlgBox()
{
    TDialog *aboutBox = new TDialog(TRect(0, 0, 42, 13), "О программе");

    aboutBox->insert(
      new TStaticText(TRect(4, 2, 39, 9),
        "\003Интерфейс конфигурирования\n\n"       // These strings will be
        "\003Версия 1.1\n"                // concatenated by the compiler.
        "\003Copyright (c) 2022\n"      // The \003 centers the line.
        "\003Театрально-технологические системы"
        )
      );

    aboutBox->insert(
      new TButton(TRect(14, 10, 26, 12), " OK", cmOK, bfDefault)
      );

    aboutBox->options |= ofCentered;

    executeDialog(aboutBox);

}


void TVDemo::chBackground()
{
    TChBackground *b = (TChBackground *) validView(new TChBackground(deskTop->background));
    if (b != 0)
    {
        deskTop->execView(b);
        destroy(b);
    }
}


//
// Change Directory function
//

void TVDemo::changeDir()
{
    TView *d = validView( new TChDirDialog( 0, cmChangeDir ) );

    if( d != 0 )
        {
        d->helpCtx = hcFCChDirDBox;
        deskTop->execView( d );
        destroy( d );
    }
}


//
// Color Control Dialog Box function
//

void TVDemo::colors()
{
    TColorGroup &group1 =
        *new TColorGroup("Рабочий стол") +
            *new TColorItem("Цвет",             1)+

        *new TColorGroup("Меню") +
            *new TColorItem("Нормальный",            2)+
            *new TColorItem("Запрещенный",          3)+
            *new TColorItem("Ссылка",          4)+
            *new TColorItem("Выбранный",          5)+
            *new TColorItem("Выбранный запрещенный", 6)+
            *new TColorItem("Выбранная ссылка", 7
        );

    TColorGroup &group2 =
        *new TColorGroup("Диалоги") +
            *new TColorItem("Рамка/фон",  33)+
            *new TColorItem("Значки рамки",       34)+
            *new TColorItem("Фон полосы прокрутки",   35)+
            *new TColorItem("Значки полосы прокрутки",  36)+
            *new TColorItem("Статический текст",       37)+

            *new TColorItem("Метка нормальная",      38)+
            *new TColorItem("Метка выбранная",    39)+
            *new TColorItem("Ускоритель метки",    40
        );

    TColorItem &item_coll1 =
        *new TColorItem("Кнопка нормальная",     41)+
        *new TColorItem("Кнопка по умолчанию",    42)+
        *new TColorItem("Кнопка выбранная",   43)+
        *new TColorItem("Кнопка отключенная",   44)+
        *new TColorItem("Ускоритель кнопки",   45)+
        *new TColorItem("Тень кнопки",     46)+
        *new TColorItem("Кластер нормальный",    47)+
        *new TColorItem("Кластер выбранный",  48)+
        *new TColorItem("Ускоритель кластера",  49
        );

    TColorItem &item_coll2 =
        *new TColorItem("Поле ввода нормальное",      50)+
        *new TColorItem("Поле ввода выбранное",    51)+
        *new TColorItem("Стрелки поля ввода",       52)+

        *new TColorItem("Кнопка истории",    53)+
        *new TColorItem("Края истории",     54)+
        *new TColorItem("Страница истории",  55)+
        *new TColorItem("Значки истории", 56)+

        *new TColorItem("Список нормальный",       57)+
        *new TColorItem("Список в фокусе",      58)+
        *new TColorItem("Список выбранный",     59)+
        *new TColorItem("Разделитель списка",      60)+

        *new TColorItem("Информационная панель",  61
        );

     group2 = group2 + item_coll1 + item_coll2;

     TColorGroup &group3 =
         *new TColorGroup("Редактор") +
             *new TColorItem("Рамка неактивного",      8)+
             *new TColorItem("Рамка активного",       9)+
             *new TColorItem("Значки рамки",       10)+
             *new TColorItem("Фон полосы прокрутки",   11)+
             *new TColorItem("Значки полосы прокрутки",  12)+
             *new TColorItem("Текст",              13);


    TColorGroup &group5 = group1 + group2 + group3;

    TColorDialog *c = new TColorDialog((TPalette*)0, &group5 );

    if( validView( c ) != 0 )
    {
        c->helpCtx = hcOCColorsDBox;  // set context help constant
        c->setData(&getPalette());
        if( deskTop->execView( c ) != cmCancel )
            {
            getPalette() = *(c->pal);
            setScreenMode(TScreen::screenMode);
            }
        destroy( c );
    }
}
